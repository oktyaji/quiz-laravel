<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
        <title>Edit Data</title>
    </head>

    <body>
        <div class="container">
            <div class="row my-5">
                <div class="col">
                    <h2>Edit Data Game</h2>
                    {{--  Code disini  --}}
                    <form action="/game/{{ $game->id }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Nama Game</label>
                            <input type="text" value="{{ $game->name }}" name="name" class="form-control" id="nama" placeholder="Nama Game">
                        </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="gameplay">Gameplay</label>
                            <textarea class="form-control" name="gameplay" id="gameplay" rows="3">{{ $game->gameplay }}</textarea>
                        </div>
                        @error('gameplay')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="developer">Developer</label>
                            <input type="text" name="developer" value="{{ $game->developer }}" class="form-control" id="developer" placeholder="Developer">
                        </div>
                        @error('developer')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="year">Tahun</label>
                            <input type="number" value="{{ $game->year }}" name="year" class="form-control" id="year" placeholder="Tahun Rilis">
                        </div>
                        @error('year')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>

        {{--  Link Gitlab: https://gitlab.com/oktyaji/quiz-laravel  --}}

        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

    </body>

</html>
